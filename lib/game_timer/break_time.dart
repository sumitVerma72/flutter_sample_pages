import 'package:flutter/material.dart';
import './game_timer.dart';

class BreakTimePage extends StatefulWidget {
  @override
  _BreakTimePageState createState() => new _BreakTimePageState();
}

class _BreakTimePageState extends State<BreakTimePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Color(0XFF152A39),
        title: new Text(
          'Womens Championship',
          style: const TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: new Flex(
        direction: Axis.vertical,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Expanded(
            child: new Stack(
              alignment: AlignmentDirectional.topCenter,
              children: <Widget>[
                new Container(
                  height: 80.0,
                  decoration: new BoxDecoration(
                      gradient: new LinearGradient(
                          colors: [Color(0XFF152A39), Color(0XFF1D567E)])),
                ),
                new Card(
                  margin: const EdgeInsets.only(top: 80.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Center(
                        child: new Text('Tournamnet is on Break '),
                      ),
                      new Center(
                        child: new Image.asset(
                          'assets/breaktime.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      new Center(
                        child: new Text(
                          'Take a deep breath to relax your mind.',
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                new Positioned(
                  top: 30.0,
                  child: new GameTimer(
                    arcBg: Colors.orange,
                    arcForeground: Colors.grey.shade300,
                    arcRadiusWidth: 10.0,
                    birbalFontSize: 20.0,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
