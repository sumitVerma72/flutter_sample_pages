import 'package:flutter/material.dart';
import './game_timer.dart';
import 'dart:math';

class GameStartPage extends StatefulWidget {
  @override
  _GameStartPageState createState() => new _GameStartPageState();
}

class _GameStartPageState extends State<GameStartPage> {
  Random random = new Random();

  Widget getPlaneBg() {
    return new Positioned(
      top: MediaQuery.of(context).size.height * random.nextDouble(),
      left: MediaQuery.of(context).size.width * random.nextDouble(),
      child: new Image.asset(
        'assets/paperplane.png',
        fit: BoxFit.cover,
        height: 100.0,
      ),
    );
  }

  Widget getCloudBg() {
    return new Positioned(
      top: MediaQuery.of(context).size.height * random.nextDouble(),
      left: MediaQuery.of(context).size.width * random.nextDouble(),
      child: new Image.asset('assets/cloud.png'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.brown,
      body: new Container(
          decoration: new BoxDecoration(
              gradient: LinearGradient(
                  begin: AlignmentDirectional.topCenter,
                  end: AlignmentDirectional.bottomCenter,
                  colors: [Color(0xff152A39), Color(0xff1D567E)])),
          child: new Stack(
            children: <Widget>[
              getPlaneBg(),
              getCloudBg(),
              getPlaneBg(),
              getCloudBg(),
              getPlaneBg(),
              getCloudBg(),
              getPlaneBg(),
              getCloudBg(),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    margin: const EdgeInsets.symmetric(vertical: 80.0),
                    child: new Column(
                      children: <Widget>[
                        new Center(
                          child: new Text(
                            'Get',
                            style: const TextStyle(
                                fontSize: 30.0,
                                fontStyle: FontStyle.italic,
                                color: Colors.white),
                          ),
                        ),
                        new Center(
                          child: new Text('Ready',
                              style: const TextStyle(
                                  fontSize: 30.0,
                                  fontStyle: FontStyle.italic,
                                  color: Colors.white)),
                        ),
                      ],
                    ),
                  ),
                  new GameTimer(
                    startTime: 10,
                    circleRadius: 150.0,
                    birbalFontSize: 30.0,
                    arcRadiusWidth: 15.0,
                    arcForeground: Colors.brown.shade100,
                    arcBg: Colors.orange,
                  ),
                  new Expanded(
                    child: new Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        new Positioned(
                          bottom: 20.0,
                          child: new Center(
                            child: new Text(
                              'We Wish you all the best !',
                              style: new TextStyle(
                                  fontSize: 20.0, color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }
}
