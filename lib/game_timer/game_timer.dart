import 'package:flutter/material.dart';
import 'dart:math' as math;

class GameTimer extends StatefulWidget {
  final int startTime;
  final double circleRadius, birbalFontSize, arcRadiusWidth;
  final Color arcBg, arcForeground;

  GameTimer(
      {this.startTime = 5,
      this.circleRadius = 100.0,
      this.birbalFontSize = 14.0,
      this.arcRadiusWidth,
      this.arcBg = Colors.pink,
      this.arcForeground = Colors.black});

  @override
  _GameTimerState createState() => new _GameTimerState(
      startTime: startTime,
      circleRadius: circleRadius,
      birbalFontSize: birbalFontSize,
      arcRadiusWidth: arcRadiusWidth,
      arcBg: arcBg,
      arcForeground: arcForeground);
}

class _GameTimerState extends State<GameTimer>
    with SingleTickerProviderStateMixin {
  final int startTime;
  final double circleRadius, birbalFontSize, arcRadiusWidth;
  final Color arcBg, arcForeground;

  _GameTimerState(
      {this.startTime,
      this.circleRadius,
      this.birbalFontSize,
      this.arcRadiusWidth,
      this.arcForeground,
      this.arcBg});

  AnimationController _aniController;

  String get timerString {
    Duration clockduration = _aniController.duration * _aniController.value;
    return '${clockduration.inSeconds.toString().padLeft(2, ' ')}';
  }

  @override
  void initState() {
    _aniController = new AnimationController(
        duration: new Duration(seconds: startTime), vsync: this);
    _aniController.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        AnimatedBuilder(
          animation: _aniController,
          builder: (BuildContext context, Widget child) {
            return new CustomPaint(
              size: new Size(circleRadius, circleRadius),
              painter: new TimerPainter(
                  animation: _aniController,
                  strokeWidth: arcRadiusWidth,
                  backgroundColor: arcForeground,
                  strokecolor: arcBg,
                  canvaSize: circleRadius),
            );
          },
        ),
        new AnimatedBuilder(
          animation: _aniController,
          builder: (context, Widget child) {
            return new Text(timerString,
                style: new TextStyle(fontSize: birbalFontSize));
          },
        ),
      ],
    );
  }
}

class TimerPainter extends CustomPainter {
  final Animation<double> animation;
  final Color backgroundColor, strokecolor;
  double canvaSize;
  double strokeWidth;
  TimerPainter(
      {this.animation,
      this.backgroundColor,
      this.strokecolor,
      this.strokeWidth = 5.0,
      this.canvaSize = 100.0})
      : super(repaint: animation);

  @override
  void paint(Canvas canvas, Size sizes) {
    Size size = Size(canvaSize, canvaSize);
    Paint paint = new Paint()
      ..color = Colors.white
      ..strokeWidth = strokeWidth
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    paint.color = strokecolor;

    Paint bgPaint = new Paint()
      ..color = backgroundColor
      ..strokeWidth = strokeWidth
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, 0.0, 360.0, false, bgPaint);
    // canvas.drawColor(strokecolor, BlendMode.color);
    canvas.drawArc(Offset.zero & size, -math.acos(0), -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        strokecolor != old.strokecolor ||
        backgroundColor != old.backgroundColor;
  }
}
