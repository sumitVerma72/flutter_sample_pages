import 'package:flutter/material.dart';

Widget customText(
    {@required String text,
    double textScaleFactor: 1.0,
    TextAlign textAlign: TextAlign.center,
    FontWeight fontweigth: FontWeight.w400,
    Color textColor: Colors.white}) {
  return new SizedBox(
    child: new FittedBox(
      child: new Text(
        text,
        textScaleFactor: textScaleFactor,
        textAlign: textAlign,
        style: TextStyle(color: textColor, fontWeight: fontweigth),
      ),
    ),
  );
}

Widget custmButton(
    {String btnText = '',
    Function method,
    Color btnColor = const Color(0xffFA6E3B),
    double horizontal = 30.0,
    FontWeight fontweigth: FontWeight.w500,
    double vertical: 13.0,
    double btnRadius = 25.0}) {
  return new Padding(
    padding: EdgeInsets.symmetric(horizontal: horizontal),
    child: new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        new Expanded(
          child: new RaisedButton(
            color: btnColor,
            splashColor: Colors.black45,
            padding: EdgeInsets.symmetric(vertical: vertical),
            shape: new RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(btnRadius),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: customText(text: btnText, fontweigth: fontweigth),
            ),
            onPressed: method,
          ),
        )
      ],
    ),
  );
}




Widget customListTile(
    {String title,
    Color titleColor = Colors.black,
    TextAlign alignTitle = TextAlign.start,
    String trailing,
    Color trailingColor = Colors.black,
    double titlefontSize = 16.0,
    double subtitlefontSize = 16.0,
    double horizontalPadding = 15.0,
    double verticalPadding = 8.0}) {
  return new Container(
    padding: EdgeInsets.symmetric(
        horizontal: horizontalPadding, vertical: verticalPadding),
    child: new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        title == null
            ? new Align()
            : new Expanded(
                child: new Text(
                  title,
                  textAlign: alignTitle,
                  style:
                      new TextStyle(color: titleColor, fontSize: titlefontSize),
                ),
              ),
        trailing == null
            ? new Align()
            : new Expanded(
                child: new Text(
                  trailing,
                  textAlign: TextAlign.center,
                  softWrap: true,
                  style: TextStyle(
                    color: trailingColor,
                    fontSize: subtitlefontSize,
                  ),
                ),
              )
      ],
    ),
  );
}