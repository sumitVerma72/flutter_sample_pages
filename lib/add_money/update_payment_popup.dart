import 'package:flutter/material.dart';
import './custom_widgets.dart';
////
/// this popup will be shown when user
///  hasn't update with payee or other details
///  essential for withdrawal
///
paymentMethodUpadteNotifierPopUp(BuildContext context,
    {popupBody:
        "In order to withdraw money from your account. Update your payment method by clicking below."}) {
  return showDialog(
      context: context,
      builder: (context) {
        double screenHeight = MediaQuery.of(context).size.height;
        return Card(
            color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.only(top: screenHeight * 0.2),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 1.5),
                          shape: BoxShape.circle,
                          color: Colors.transparent),
                      child: IconButton(
                        icon: Icon(
                          Icons.close,
                          size: 26.0,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 260.0,
                    child: Card(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 16.0, vertical: 16.0),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                "Update Payment Method",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.purple),
                              ),
                            ),
                            Text(
                              popupBody,
                              style: TextStyle(color: Colors.brown),
                              textScaleFactor: 1.1,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 7.0),
                              child: custmButton(
                                  btnText: "UPDATE PAYEE INFORMATION",
                                  method: () {
                                    // Navigator.of(context).push(
                                    //     new MaterialPageRoute(
                                    //         builder: (context) =>
                                    //             new PlayerProfile(
                                    //               tabIndex: 2,
                                    //             )));
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ));
      });
}
