import 'package:flutter/material.dart';
import './custom_widgets.dart';
import 'update_payment_popup.dart';

const rupeetextIcon = '\u20B9';

class GameSelectWithdrawalMethod extends StatefulWidget {
  @override
  _GameSelectWithdrawalMethodState createState() =>
      new _GameSelectWithdrawalMethodState();
}

List<String> bankDetails = [
  "HDFC Bank Account",
  "Savings Account - 358174923",
  "IFSC - HDFC1234567"
];
List<String> paytmDetails = ["Mobile No - 8553110962"];
List<String> upiDetails = ["dhruvanwd@okhdfc"];

class _GameSelectWithdrawalMethodState
    extends State<GameSelectWithdrawalMethod> with TickerProviderStateMixin {
  TextEditingController moneycontroller = new TextEditingController();
  String withdrawBtnTxt = 'Withdraw to Bank';
  AnimationController bnkCardcontroller;
  AnimationController paytmCardcontroller;
  AnimationController upiCardcontroller;
  ScrollController pagecontroller;

  getInfoCard(List<String> payInfo) {
    final List<Widget> infoList = new List();
    int index = 0;
    payInfo.forEach((info) {
      infoList.add(customListTile(
        titleColor: index != 1 ? Colors.grey.shade700 : Colors.black,
        title: info,
      ));
      index++;
    });
    infoList.add(Padding(
      padding: const EdgeInsets.only(bottom: 15.0, top: 10.0),
      child: custmButton(
          btnText: "CONTINUE",
          horizontal: 30.0,
          method: () {
            ///
            /// popup on successfull transaction
            ///

            // popDialogueBox(context, {
            //   "title": "Withdrawal Request Successful",
            //   "content": "Dear AAAA1992, \n"
            //       'It is always wise to face up than holding back. Your '
            //       'withdrawal reversal request has been approved and cash'
            //       'has been added to your Birbal Quiz account. Please'
            //       'find below your transaction details.'
            //       'Transaction Details:-'
            //       'Transaction ID: 13110566290118101323'
            //       'Transaction Amount: Rs. 6100.00'
            //       'Transaction Date: 30/01/2018'
            //       ' Transaction Time: 13:43:11'
            //       ' With any additional questions, please contact our banking'
            //       ' team at banking@birbalquiz.com.'
            //       'Enjoy playing at Birbal Quiz'
            // });

            //popup if payment method is not updated

            paymentMethodUpadteNotifierPopUp(context);
          }),
    ));
    return new Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Column(
        children: infoList,
      ),
    );
  }

  initState() {
    super.initState();
    pagecontroller = new ScrollController();
    bnkCardcontroller = AnimationController(
        duration: const Duration(microseconds: 100), vsync: this);
    paytmCardcontroller = AnimationController(
        duration: const Duration(microseconds: 100), vsync: this);
    upiCardcontroller = AnimationController(
        duration: const Duration(microseconds: 100), vsync: this);
  }

  dispose() {
    bnkCardcontroller.dispose();
    paytmCardcontroller.dispose();
    upiCardcontroller.dispose();
    super.dispose();
  }

  setRadioState(int i) {
    print(i);
    setState(() {});
    if (i == 0) {
      withdrawBtnTxt = 'Withdraw to Bank';
      paytmCardcontroller.reverse();
      bnkCardcontroller.forward();
      upiCardcontroller.reverse();
    } else if (i == 1) {
      withdrawBtnTxt = 'Withdraw using Paytm';
      paytmCardcontroller.forward();
      bnkCardcontroller.reverse();
      upiCardcontroller.reverse();
    } else {
      withdrawBtnTxt = 'Withdraw using UPI';
      bnkCardcontroller.reverse();
      paytmCardcontroller.reverse();
      upiCardcontroller.forward();
    }
    // pagecontroller.jumpTo(0.0);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Withdraw Cash'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: new SingleChildScrollView(
        controller: pagecontroller,
        child: new Flex(
          direction: Axis.vertical,
          children: <Widget>[
            new Card(
              child: new ListTile(
                leading: new Image.asset('assets/piggybank.png'),
                title: new Text('Available Balance'),
                trailing: new Text(
                  '$rupeetextIcon 100',
                  style: const TextStyle(color: Colors.orange),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Text('Enter Withdrawal Amount'),
            ),
            new Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: new SizedBox(
                width: 150.0,
                child: new TextField(
                  controller: moneycontroller,
                  decoration: const InputDecoration(
                      prefixText: rupeetextIcon,
                      prefixStyle:
                          TextStyle(fontSize: 18.0, color: Colors.orange)),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  autofocus: true,
                ),
              ),
            ),
            new FlatButton(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: new Row(
                children: <Widget>[
                  new Image.asset(
                    'assets/bank_bg.png',
                    height: 24.0,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: new Text(
                      'Online Bank Transfer',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.red.shade900,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
              onPressed: () {
                setRadioState(0);
              },
            ),
            new Divider(),
            new SizeTransition(
                sizeFactor: bnkCardcontroller, child: getInfoCard(bankDetails)),
            new FlatButton(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: new Row(
                children: <Widget>[
                  new Image.asset(
                    'assets/paytm.png',
                    height: 24.0,
                    width: 30.0,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: new Text(
                      'Paytm Wallet Transfer',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.red.shade900,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
              onPressed: () {
                setRadioState(1);
              },
            ),
            new Divider(),
            new SizeTransition(
                sizeFactor: paytmCardcontroller,
                child: getInfoCard(paytmDetails)),
            new FlatButton(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: new Row(
                children: <Widget>[
                  new Image.asset(
                    'assets/bhim.png',
                    height: 24.0,
                    width: 30.0,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: new Text(
                      'UPI Transfer',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.red.shade900,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
              onPressed: () {
                setRadioState(2);
              },
            ),
            new SizeTransition(
                sizeFactor: upiCardcontroller, child: getInfoCard(upiDetails)),
          ],
        ),
      ),
    );
  }
}
