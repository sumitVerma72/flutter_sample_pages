import 'package:flutter/material.dart';
import 'dart:async';

import './custom_widgets.dart';

const rupeetextIcon = '\u20B9';

class GameAddCash extends StatefulWidget {
  @override
  _GameAddCashState createState() => _GameAddCashState();
}

List<Map<String, String>> coupons = [
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  },
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  },
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  },
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  },
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  },
  {
    "code": "DEPOSITORFREEROLL52",
    "description":
        "Avail 100% bonus on your first deposit up-to maximum of Rs.10000 (Offer valid till 30 Apr 18 and only on your first cash deposit)",
  }
];

class _GameAddCashState extends State<GameAddCash> {
  TextEditingController moneycontroller = TextEditingController();

  Widget getchip(String info, Function method) {
    return ActionChip(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      label: Text(
        info,
        textScaleFactor: 1.1,
      ),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
          side: BorderSide(color: Colors.deepOrange.shade900)),
      onPressed: method,
    );
  }

  int radiogroup = 1;

  selectPaymentMethod(int value) {
    setState(() {
      radiogroup = value;
    });
  }

  Future<Widget> popDialogueBox(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return CouponsList();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text('Add Cash'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.90,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Card(
                child: ListTile(
                  leading: Image.asset('assets/piggybank.png'),
                  title: Text('Available Balance'),
                  trailing: Text(
                    'RS 100',
                    style: const TextStyle(color: Colors.orange),
                  ),
                ),
              ),
              Center(
                child: FittedBox(
                  child: SizedBox(
                    child: Text("Enter Amount"),
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: 150.0,
                  child: TextField(
                    controller: moneycontroller,
                    decoration: const InputDecoration(
                        prefixText: rupeetextIcon,
                        prefixStyle:
                            TextStyle(fontSize: 18.0, color: Colors.orange)),
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.center,
                    autofocus: true,
                  ),
                ),
              ),
              ButtonBar(
                alignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  getchip('+50', () {
                    print(moneycontroller.text);
                    if (moneycontroller.text.length > 0) {
                      moneycontroller.text =
                          '${int.parse(moneycontroller.text) + 50}';
                    } else {
                      moneycontroller.text = '50';
                    }
                  }),
                  getchip('+100', () {
                    print(moneycontroller.text);
                    if (moneycontroller.text.length > 0) {
                      moneycontroller.text =
                          '${int.parse(moneycontroller.text) + 100}';
                    } else {
                      moneycontroller.text = '100';
                    }
                  }),
                  getchip('+200', () {
                    print(moneycontroller.text);
                    if (moneycontroller.text.length > 0) {
                      moneycontroller.text =
                          '${int.parse(moneycontroller.text) + 200}';
                    } else {
                      moneycontroller.text = '200';
                    }
                  }),
                ],
              ),
              FlatButton(
                child: Text(
                  'Have a Promo Code ?',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.orange),
                ),
                onPressed: () {
                  popDialogueBox(context);
                },
              ),
              Flexible(
                child: SingleChildScrollView(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 1,
                              groupValue: radiogroup,
                              title: Text('Net Banking'),
                              onChanged: selectPaymentMethod,
                            ),
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 2,
                              groupValue: radiogroup,
                              title: Text('Debit Card'),
                              onChanged: selectPaymentMethod,
                            ),
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 3,
                              groupValue: radiogroup,
                              title: Text('UPI'),
                              onChanged: selectPaymentMethod,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 4,
                              groupValue: radiogroup,
                              title: Text('Credit Card'),
                              onChanged: selectPaymentMethod,
                            ),
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 5,
                              groupValue: radiogroup,
                              title: Text('Paytm'),
                              onChanged: selectPaymentMethod,
                            ),
                            RadioListTile(
                              dense: true,
                              activeColor: Colors.orange,
                              value: 6,
                              groupValue: radiogroup,
                              title: Text('Other Wallets'),
                              onChanged: selectPaymentMethod,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional.bottomCenter,
                child: custmButton(
                  btnText: 'ADD MONEY',
                  method: () {
                    popDialogueBox(context);
                  },
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: customText(
                      text:
                          'Money will be added to your KwizKing Quiz account.',
                      textColor: Colors.brown.shade600),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CouponsList extends StatefulWidget {
  @override
  _CouponsListState createState() => _CouponsListState();
}

class _CouponsListState extends State<CouponsList> {
  int selectedCoupon = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 50.0),
            child: SizedBox(
              height: 50.0,
              child: Container(
                child: IconButton(
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 28.0,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.transparent,
                    border: Border.all(color: Colors.white)),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
              margin: const EdgeInsets.only(
                  left: 15.0, right: 15.0, bottom: 60.0, top: 15.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: coupons.length,
                      itemBuilder: (context, index) {
                        return coupons.length == 0
                            ? Text('No Coupons For You')
                            : RadioListTile(
                                dense: true,
                                title: Text(coupons[index]['code']),
                                subtitle: Text(coupons[index]['description']),
                                value: index,
                                groupValue: selectedCoupon,
                                onChanged: (ind) {
                                  print(ind);
                                  setState(() {
                                    selectedCoupon = ind;
                                  });
                                },
                              );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: custmButton(btnText: 'APPLY', method: () {}),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
