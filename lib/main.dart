import 'package:flutter/material.dart';
import 'game_timer/game_start.dart';
import 'game_timer/break_time.dart';
import 'add_money/add_cash.dart';
import 'add_money/select_transaction_method.dart';
import 'image_slider/game_tutorial.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Sumit_Workspace',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      // to run canvas files use "flutter run -t xxy/yyy.dart"
      //ex: flutter run -t lib/canvas/canvas_ui.dart
      home: PagesList(),
    );
  }
}

class PagesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pages List"),
      ),
      body: ListView.custom(
        childrenDelegate: SliverChildListDelegate([
          ListTile(
            leading: CircleAvatar(
              child: Text("1"),
            ),
            title: Text("Game Start page"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => GameStartPage()));
            },
          ),
          ListTile(
            leading: CircleAvatar(
              child: Text("2"),
            ),
            title: Text("Add Cash page"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => GameAddCash()));
            },
          ),
          ListTile(
            leading: CircleAvatar(
              child: Text("3"),
            ),
            title: Text("Game Beak page"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => BreakTimePage()));
            },
          ),
          ListTile(
            leading: CircleAvatar(
              child: Text("4"),
            ),
            title: Text("Select Transaction type page"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      GameSelectWithdrawalMethod()));
            },
          ),
          ListTile(
            leading: CircleAvatar(
              child: Text("5"),
            ),
            title: Text("Image Slider page"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => GameTutorial()));
            },
          ),
        ]),
      ),
    );
  }
}
