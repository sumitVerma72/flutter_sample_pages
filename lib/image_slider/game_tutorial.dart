import 'package:flutter/material.dart';
import './birbalImageSlider.dart';

class GameTutorial extends StatefulWidget {
  final List<String> images = [
    'assets/test1.jpg',
    'assets/test2.jpg',
    'assets/test3.jpg',
    'assets/test4.jpg',
    'assets/test5.jpg',
    'assets/test6.jpg',
  ];

  @override
  _GameTutorialState createState() => _GameTutorialState();
}

PageController pageController =
    new PageController(viewportFraction: 0.8, initialPage: 100);

class _GameTutorialState extends State<GameTutorial>
    with SingleTickerProviderStateMixin {
  int currentIndicator = 0;
  Size cardSize = Size(300.0, 250.0);

  //game string image name to image converter
  List<Widget> getImageList() {
    List<Widget> imageAsset = [];
    widget.images.forEach((image) {
      imageAsset.add(new Image.asset(
        image,
        fit: BoxFit.cover,
        height: cardSize.height,
        width: cardSize.width,
      ));
    });
    return imageAsset;
  }

  @override
  void initState() {
    // TODO: implement initState
    pageController.addListener(() {
      if (currentIndicator > currentPage) moveForeward();
      movePrevious();
    });
    super.initState();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   pageController.dispose();
  // }

  moveForeward() {
    print(currentPage);
    setState(() {
      currentIndicator < 4 ? ++currentIndicator : currentIndicator = 0;
    });
  }

  movePrevious() {
    print(currentPage);
    setState(() {
      if (currentIndicator <= 0) {
        currentIndicator = 5;
      }
      --currentIndicator;
    });
  }

  Widget indicator(
      {double height: 15.0,
      @required int currentIndex,
      @required Color color}) {
    return new Card(
      color: color,
      shape:
          new RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
      child: new SizedBox(
        width: currentIndex == currentIndicator ? height * 2.0 : height,
        height: height,
      ),
    );
  }

  getBullets() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        indicator(color: Colors.grey, currentIndex: 0),
        indicator(color: Colors.red, currentIndex: 1),
        indicator(color: Colors.lime, currentIndex: 2),
        indicator(color: Colors.pink, currentIndex: 3),
        indicator(color: Colors.orange, currentIndex: 4),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screensize = MediaQuery.of(context).size;
    cardSize = Size(screensize.width, screensize.height * 0.41);

    ///
    ///
    ///
    ///
    return Scaffold(
      backgroundColor: Colors.blue,
      body: new Container(
          child: new Column(
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(top: 40.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.symmetric(vertical: 1.0),
                  child: Text(
                    'Image Slider',
                    textAlign: TextAlign.center,
                    textScaleFactor: 2.0,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w500),
                  ),
                )
              ],
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(10.0),
            child: new Text(
              'In Pursuit of happiness',
              textAlign: TextAlign.right,
              textScaleFactor: 1.2,
              style: TextStyle(color: Colors.white),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: new CarouselSlider(
                items: getImageList(),
                pageController: pageController,
                viewportFraction: 0.8,
                initialPage: 0,
                height: cardSize.height,
                width: cardSize.width,
                reverse: true,
                autoPlay: false,
                interval: const Duration(seconds: 4),
                autoPlayCurve: Curves.fastOutSlowIn,
                autoPlayDuration: const Duration(milliseconds: 800)),
          ),
          new Container(
            child: new Text(
              'Earn Money',
              textScaleFactor: 1.3,
              style: TextStyle(
                  color: Colors.redAccent.shade400,
                  fontWeight: FontWeight.w500),
            ),
            padding: const EdgeInsets.symmetric(vertical: 5.0),
          ),
          new Expanded(
            child: new Container(
              child: new Center(
                child: new SizedBox(
                  width: 300.0,
                  child: new Text(
                    'Lorem ipsum dolor sit amet,'
                        'consectetuer adipiscing elit, sed diam'
                        'nonummy nibh euismod tincidunt',
                    textAlign: TextAlign.center,
                    textScaleFactor: 1.1,
                  ),
                ),
              ),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: getBullets(),
          ),
          new Padding(
            padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: new FlatButton(
              child: const Text(
                'Skip',
                textScaleFactor: 1.2,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          )
        ],
      )),
    );
  }
}
