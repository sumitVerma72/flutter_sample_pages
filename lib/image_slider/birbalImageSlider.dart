library carousel_slider;

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

int _remander(int input, int source) {
  final int result = input % source;
  return result < 0 ? source + result : result;
}

int _getRealIndex(int position, int base, int length) {
  final int offset = position - base;
  return _remander(offset, length);
}

int currentPage;

class CarouselSlider extends StatefulWidget {
  final List<Widget> items;
  final num viewportFraction;
  final num initialPage;
  final double height;
  final PageController pageController;

  final double width;
  final num realPage;
  final bool autoPlay;
  final Duration autoPlayDuration;
  final Curve autoPlayCurve;
  final Duration interval;
  final bool reverse;

  CarouselSlider(
      {@required this.items,
      @required this.height,
      @required this.width,
      this.viewportFraction: 0.8,
      @required this.pageController,
      this.initialPage: 0,
      this.realPage: 10000,
      this.autoPlay: false,
      this.interval: const Duration(seconds: 4),
      this.reverse: false,
      this.autoPlayCurve: Curves.fastOutSlowIn,
      this.autoPlayDuration: const Duration(milliseconds: 800)});

  @override
  _CarouselSliderState createState() {
    return new _CarouselSliderState();
  }
}

class _CarouselSliderState extends State<CarouselSlider>
    with TickerProviderStateMixin {
  Timer timer;

  @override
  void initState() {
    super.initState();
    currentPage = widget.initialPage;
    widget.pageController.addListener(() {
      print(currentPage);
    });
    if (widget.autoPlay) {
      timer = new Timer.periodic(widget.interval, (_) {
        widget.pageController.nextPage(
            duration: widget.autoPlayDuration, curve: widget.autoPlayCurve);
      });
    }
  }

  getWrapper(Widget child) {
    return new Container(
        height: widget.height, width: widget.width, child: child);
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   timer?.cancel();
  // }

  @override
  Widget build(BuildContext context) {
    return getWrapper(new PageView.builder(
      onPageChanged: (int index) {
        currentPage =
            _getRealIndex(index, widget.realPage, widget.items.length);
      },
      controller: widget.pageController,
      reverse: widget.reverse,
      itemBuilder: (BuildContext context, int i) {
        final int index =
            _getRealIndex(i, widget.realPage, widget.items.length);

        return new AnimatedBuilder(
            animation: widget.pageController,
            builder: (BuildContext context, child) {
              // on the first render, the widget.pageController.page is null,
              // this is a dirty hack
              if (widget.pageController.position.minScrollExtent == null ||
                  widget.pageController.position.maxScrollExtent == null) {
                new Future.delayed(new Duration(microseconds: 1), () {
                  setState(() {
                    print(widget.pageController);
                  });
                });
                return new Container();
              }
              double value = widget.pageController.page - i;
              value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);

              final double height = widget.height;

              return new Center(
                  child: new SizedBox(
                      height: Curves.easeOut.transform(value) * height,
                      width: widget.width,
                      child: new Card(
                        elevation: 5.0,
                        child: child,
                      )));
            },
            child: widget.items[index]);
      },
    ));
  }
}
